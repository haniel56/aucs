# AUCS Core Repository

Central point of information for Aberdeen University Computing Society and the core for our projects.
 
## Server

The server address is `34.247.173.13`.

You can connect by `ssh aucs@34.247.173.13`.

### How to gain access

In order to gain access, you need to add your public SSH-RSA key into `./members`.

#### If you get a "permission denied" error

Make sure the right public key is in `./members`.
Note that updating the member information can take a minute to propagate.

## Committing Process

In order to make changes, you can either request access to create your own branches or fork the repository.

You can then make the desired changes in your local copy of the repository,
after which you have to submit a pull request to merge your changes.
